class CreateStudyYears < ActiveRecord::Migration
  def change
    create_table :study_years do |t|
      t.integer :value 
      t.timestamps null: false
    end
  end
end
