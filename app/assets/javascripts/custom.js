

var id_counter = 0;
var results = {};



$(document).ready(function(){
  populateModalSelects();
  $('[data-tooltip="true"]').tooltip(); 
  $(':checkbox').checkboxpicker();
});


function populateModalSelects() {
  var teachers_select = $('#js-teacher-select');

  for (var i = 0; i < teachers.length; i++) {
    console.log(teachers[i].name)
    var option = $('<option></option>');
    option.attr("value", teachers[i].name);
    option.text(teachers[i].name);
    teachers_select.append(option);
  }

  var subjects_select = $('#js-subject-select');

  for (var i = 0; i < subjects.length; i++) {
    var option = $('<option></option>');
    option.attr("value", subjects[i].short_title);
    option.text(subjects[i].short_title);
    subjects_select.append(option);
  }

  var rooms_select = $('#js-room-select');

  for (var i = 0; i < rooms.length; i++) {
    var option = $('<option></option>');
    option.attr("value", rooms[i].id);
    option.text(rooms[i].id);
    rooms_select.append(option);
  }
}


function repopulateSelect(select, options) {
  select.empty();

  for (var i = 0; i < options.length; i++) {
    select.append($('<option></option>').text(options[i]).attr('value', options[i]));
  }
}


function filterCurrentOptions(btnModalSave, currentButton) {
  var teachers_select = $('#js-teacher-select');
  var rooms_select = $('#js-room-select');

  var id = currentButton.attr('data-id');

  var matching = _.where(results, {week_day: btnModalSave.attr('data-save-week-day'), hour: btnModalSave.attr('data-save-hour-id')});

  var teacher_options = _.difference(_.pluck(teachers, 'name'), _.pluck(matching, 'teacher'));
  var room_options = _.difference(_.pluck(rooms, 'id'), _.map(matching, function(r) {return parseInt(r.room);}));

  if (id != -1) {
    var currentTeacher = results[id].teacher;
    var currentRoom = parseInt(results[id].room); 

    teacher_options = _.union([currentTeacher], teacher_options);
    room_options = _.union([currentRoom], room_options);
  } 

  repopulateSelect(teachers_select, teacher_options);
  teachers_select.selectpicker('refresh');

  repopulateSelect(rooms_select, room_options);
  rooms_select.selectpicker('refresh');
}


$(document).on('click', 'button.btn[data-target="#editModal"]',function() {
  var currentButton = $(this);
  $('[data-tooltip="true"]').tooltip('hide');

  var btnModalSave = $('#btn-modal-save');
  btnModalSave.attr('data-save-week-day', currentButton.attr('data-week-day')).attr('data-save-hour-id', currentButton.attr('data-hour-id')).attr('data-group', currentButton.attr('data-group'));

  var btnModalDelete = $('#btn-modal-delete');
  btnModalDelete.attr('data-id', currentButton.attr('data-id'));

  filterCurrentOptions(btnModalSave, currentButton);
});


$(document).on('click', '#btn-modal-save', function() {
  var week_day = $(this).attr('data-save-week-day');
  var hour_id = $(this).attr('data-save-hour-id');
  var group = $(this).attr('data-group');
  var currentButton = $('button[data-hour-id=' + hour_id + '][data-week-day=' + week_day + '][data-group=' + group + ']');

  var teacher_name =  $('#js-teacher-select').val();
  var subject_title =  $('#js-subject-select').val();
  var room_id =  $('#js-room-select').val();

  var result;

  if (currentButton.attr('data-is-empty') == 'true'){
    result = new Result({
      subject: subject_title,
      teacher: teacher_name,
      week_type: "odd",
      week_day: currentButton.attr('data-week-day'),
      hour: hour_id,
      room: room_id,
      group: group
    });

    results[id_counter] = result;
    currentButton.replaceWith(result.renderAsButton(id_counter));

    id_counter++;

  } else {
    result = results[currentButton.attr('data-id')];
    result['subject'] = subject_title;
    result['teacher'] = teacher_name;
    result['room'] = room_id;
    result.rerenderAsButton(currentButton);
  }

  $('[data-tooltip="true"]').tooltip();

  $('#subject-type').submit();
});


$(document).on('click', '#btn-modal-delete', function() {
  var id = $(this).attr('data-id');

  if (id != -1) {
    var calling_button = $('button.js-btn-single-result[data-id=' + id + ']');
    delete results[id.toString()];
    var new_button = newEmptyButton(calling_button.data('weekDay'), calling_button.data('hourId'), calling_button.data('group'));
    calling_button.replaceWith(new_button);
  }
});


$(document).submit('#subject-type', function(e) {
  e.preventDefault();
  console.log($(this).find('[name=subjectTypeRadios]').val());
});


function newEmptyButton(week_day, hour_id, group) {
  var btn = $('<button></button>').addClass('btn').addClass('btn-success').addClass('js-btn-no-result');
  btn.attr('data-id', -1).attr('data-toggle', 'modal').attr('data-target', '#editModal').attr('data-is-empty', 'true');
  btn.attr('data-week-day', week_day).attr('data-hour-id', hour_id).attr('data-group', group);
  btn.text('Available');
  return btn;
} 


$('#btn-set-busy').on("click", function(){
  console.log('pressed'); 

  $('td > button[type="checkbox"]').each(function(btn) {
    var currentButton = $(this);

    if (currentButton.attr('aria-pressed') == false && currentButton.attr('data-busy') == false) {
      var busyButton = $('<button></button');

      busyButton.addClass('btn').addClass('btn-danger');
      busyButton.attr('type', 'checkbox').attr('data-toggle', 'button').attr('aria-pressed', 'false').attr('data-busy', 'true');
      busyButton.text('Busy');

      currentButton.replaceWith(busyButton);
    }
  });
});

// localStorage.setItem('rooms', JSON.stringify(rooms));
// localStorage.setItem('teachers', JSON.stringify(teachers));
// localStorage.setItem('subjects', JSON.stringify(subjects));
// localStorage.setItem('results', JSON.stringify(results));


//<td><button class="btn btn-danger" data-toggle="tooltip" title="SI, A. Railean, 202 - APPOO, I. Zarea, 201">Collision</button></td>
