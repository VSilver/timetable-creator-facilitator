function Result(obj) {
  this.subject = obj.subject
  this.teacher = obj.teacher
  this.week_day = obj.week_day
  this.week_type = obj.week_type
  this.hour = obj.hour
  this.room = obj.room
  this.group = obj.group
}

Result.prototype.renderAsButton = function(id) {
  var button = $('<button></button>')

  button.addClass('btn').addClass('btn-default')
  button.addClass('js-btn-single-result')
  button.attr("data-toggle", "modal")
  button.attr("data-tooltip", "true")
  button.attr("data-id", id)
  button.attr('data-is-empty', 'false')
  button.attr('data-week-day', this.week_day)
  button.attr('data-hour-id', this.hour)
  button.attr('data-group', this.group)
  button.attr("title", this.subject + ' - ' + this.teacher + ', ' + this.room + ', ' + this.hour)
  button.text(this.subject + ' - ' + this.teacher)
  button.attr("data-target", "#editModal")

  return button
};


Result.prototype.rerenderAsButton = function(button) {
  button.attr("data-tooltip", "true")
  button.attr("data-original-title", this.subject + ' - ' + this.teacher + ', ' + this.room + ', ' + this.week_day)
  button.text(this.subject + ' - ' + this.teacher)
};


function Teacher(obj) {
  this.id = obj.id
  this.name = obj.name
  this.courses = obj.courses
  this.availability = obj.availability
}

Teacher.prototype.addCourse = function(course) {
  this.courses.push(course)
}


Teacher.prototype.removeCourse = function(course) {
  var index = this.courses.indexOf(course)
  if (index != -1) {
    this.course.splice(index, 1)
  }
}


Teacher.prototype.changeAvailability(availability) {
  this.availability = availability
}


function Curriculum(obj) {
  this.id = obj.id
  this.semester = obj.semester
  this.specialty = obj.specialty
  this.course_hours = obj.course_hours
}


Curriculum.prototype.addCourseHour = function(obj) {
  this.course_hours.push(obj)
}


function Specialty(obj) {
  this.id = obj.id
  this.name = this.name
  this.curriculums = obj.curriculums
}

Specialty.prototype.addCurriculum = function(curriculum, semester) {
  this.curriculums[semester] = curriculum
}


function Room(obj) {
  this.id = obj.id
  this.type = obj.type
  this.capacity = obj.capacity
}


function Group(obj) {
  this.id = obj.id
  this.specialty = obj.specialty
  this.results = obj.results
  this.nr_students = obj.nr_students
  this.semester = obj.semester
}


function Subject(obj) {
  this.id = obj.id
  this.short_title = obj.short_title
  this.long_title = obj.long_title
}


var teachers = [
  new Teacher({"id": 1, "name": "I. Zarea", "courses": []}),
  new Teacher({"id": 4, "name": "V. Bostan", "courses": []}),
  new Teacher({"id": 2, "name": "A. Railean", "courses": []}),
  new Teacher({"id": 3, "name": "D. Ciorba", "courses": []})
];


var subjects = [
  new Subject({"id": 1, "short_title": "APPOO", "long_title": "Analiza si Proiectarea Programelor Obiect Orientate"}),
  new Subject({"id": 2, "short_title": "SI", "long_title": "Securitatea Informationala"}),
  new Subject({"id": 3, "short_title": "PAD", "long_title": "Proiectarea Aplicatiilor Distribuite"}),
  new Subject({"id": 4, "short_title": "TPI", "long_title": "Teoria Probabilitatii si a Informatiei"})
];

var rooms = [ 
  new Room({"id": 201, "floor": 2, "capacity": 30, "type": "laboratory"}),
  new Room({"id": 202, "floor": 2, "capacity": 30, "type": "lecture"}),
  new Room({"id": 203, "floor": 2, "capacity": 20, "type": "laboratory"})
];

var specialties = [
  new Specialty({"id": 1, "name": TI, "curriculum": {"7": new Curriculum({"semester": "7", "course_hours": []})}}),
  new Specialty({"id": 1, "name": IA, "curriculum": {"7": new Curriculum({"semester": "7", "course_hours": []})}}),
  new Specialty({"id": 1, "name": SI, "curriculum": {"7": new Curriculum({"semester": "7", "course_hours": []})}})
];

var groups = [
  new Group({"id": 1, "name": "TI-121", "semester": "7", "nr_students": 27, "specialty": specialties[0]}),
  new Group({"id": 1, "name": "FI-121", "semester": "7", "nr_students": 27, "specialty": specialties[0]}),
  new Group({"id": 1, "name": "SI-121", "semester": "7", "nr_students": 25, "specialty": specialties[2]}),
  new Group({"id": 1, "name": "FAF-121", "semester": "7", "nr_students": 24, "specialty": specialties[0]})
];


function populateSpecialties() {
  var TI = specialties[0]
  var SI = specialties[1]

  var APPOO = subjects[0]
  var InfoSec = subjects[1]
  var PAD = subjects[2]
  var TPI = subjects[3]

  TI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "lecture", "nr_hours": 3});
  TI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "laboratory", "nr_hours": 2});
  TI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "seminar", "nr_hours": 2});
  TI.curriculum["7"].addCourseHour({"subject": InfoSec, "type": "lecture", "nr_hours": 2})
  TI.curriculum["7"].addCourseHour({"subject": InfoSec, "type": "laboratory", "nr_hours": 2})
  TI.curriculum["7"].addCourseHour({"subject": PAD, "type": "lecture", "nr_hours": 3});
  TI.curriculum["7"].addCourseHour({"subject": PAD, "type": "laboratory", "nr_hours": 2});
  TI.curriculum["7"].addCourseHour({"subject": PAD, "type": "seminar", "nr_hours": 2});
  TI.curriculum["7"].addCourseHour({"subject": TPI, "type": "lecture", "nr_hours": 3});
  TI.curriculum["7"].addCourseHour({"subject": TPI, "type": "laboratory", "nr_hours": 2});
  TI.curriculum["7"].addCourseHour({"subject": TPI, "type": "seminar", "nr_hours": 2});

  SI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "lecture", "nr_hours": 3});
  SI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "laboratory", "nr_hours": 2});
  SI.curriculum["7"].addCourseHour({"subject": APPOO, "type": "seminar", "nr_hours": 2});
  SI.curriculum["7"].addCourseHour({"subject": InfoSec, "type": "lecture", "nr_hours": 2})
  SI.curriculum["7"].addCourseHour({"subject": InfoSec, "type": "laboratory", "nr_hours": 2})
  SI.curriculum["7"].addCourseHour({"subject": PAD, "type": "lecture", "nr_hours": 3});
  SI.curriculum["7"].addCourseHour({"subject": PAD, "type": "laboratory", "nr_hours": 2});
  SI.curriculum["7"].addCourseHour({"subject": PAD, "type": "seminar", "nr_hours": 2});
}




